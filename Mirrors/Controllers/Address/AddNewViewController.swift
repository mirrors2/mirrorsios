//
//  AddNewViewController.swift
//  Mirrors
//
//  Created by apple on 24/07/21.
//

import UIKit
import GoogleMaps
import GooglePlacePicker
import GooglePlaces

class AddNewViewController: UIViewController,GMSMapViewDelegate {

    @IBOutlet weak var searchText: UITextField!
    var formatAddress = ""
    
    var lat:String = ""
    var long:String = ""
    
    
    @IBOutlet weak var searcButton: UIButton!
    
    @IBOutlet weak var home: UIButton!
    
    //location manager for accessing current location of the device
    private let locationManager = CLLocationManager()
    
    @IBOutlet weak var gmsMapView: GMSMapView!
    @IBOutlet weak var office: UIButton!
    @IBOutlet weak var other: UIButton!
    @IBOutlet weak var change: UIButton!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var viewMember: UIButton!
    
    @IBOutlet weak var addree2: UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var contiu2: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        
        home.backgroundColor = #colorLiteral(red: 1, green: 0.6078431373, blue: 0.631372549, alpha: 1)
        home.setTitleColor(.white, for: UIControl.State.normal)
        
        office.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        office.titleLabel?.textColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        
        other.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        other.titleLabel?.textColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        
        
        name.layer.cornerRadius = 6
        name.layer.borderColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        name.layer.borderWidth = 0.5

        addree2.layer.cornerRadius = 6
        addree2.layer.borderColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        addree2.layer.borderWidth = 0.5

        viewMember.layer.cornerRadius = 6
        contiu2.layer.cornerRadius = 10
        textFieldSearch.layer.cornerRadius = 6
        change.layer.cornerRadius = 6
        
        layoutSubviews()
        
        other.layer.cornerRadius = 8
        home.layer.cornerRadius = 8
        office.layer.cornerRadius = 8
        // Do any additional setup after loading the view.
    }
   // d9d9d9//
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        searchText.text = formatAddress
        locateWithLong(lon: lat ?? "", andLatitude: long ?? "", andAddress: formatAddress)
        
    }
    @IBAction func searchAddress(_ sender: Any) {
        
        let naVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchAddressViewController") as! SearchAddressViewController
        
        navigationController?.pushViewController(naVC, animated: true)
        
        
        
        
        
        
        
        
    }
    
    
    
    let marker = GMSMarker()
    func locateWithLong(lon: String, andLatitude lat: String, andAddress address: String) {
        DispatchQueue.main.async {
            
            let latDouble = Double(lat)
            let lonDouble = Double(lon)
           
      print("lat is",self.long)
            print("long is",lonDouble)
            let center = CLLocationCoordinate2D(latitude: lonDouble ?? 20.176767, longitude: latDouble ?? 82.1916628)

            let camera = GMSCameraPosition.camera(withLatitude: lonDouble ?? 20.176767, longitude: latDouble ?? 82.1916628, zoom: 15)
            self.gmsMapView.camera = camera
            

           
            self.marker.map = self.gmsMapView
            self.marker.position = CLLocationCoordinate2D(latitude: lonDouble ?? 20.176767, longitude: latDouble ?? 82.1916628)
            self.marker.title = "\(address)"
            
            
            
        }
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
      marker.position = position.target
      print(marker.position.latitude)
      
      
      
    }
    
    @IBAction func home(_ sender: Any) {
        home.backgroundColor = #colorLiteral(red: 1, green: 0.6078431373, blue: 0.631372549, alpha: 1)
        home.setTitleColor(.white, for: UIControl.State.normal)
        
        office.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        office.titleLabel?.textColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        
        other.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        other.titleLabel?.textColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        
        
    }
    
    @IBAction func office(_ sender: Any) {
        
        office.backgroundColor = #colorLiteral(red: 1, green: 0.6078431373, blue: 0.631372549, alpha: 1)
        office.setTitleColor(.white, for: UIControl.State.normal)
        
        home.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        home.titleLabel?.textColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        
        other.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        other.titleLabel?.textColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
    }
    
    @IBAction func other(_ sender: Any) {
        other.backgroundColor = #colorLiteral(red: 1, green: 0.6078431373, blue: 0.631372549, alpha: 1)
        other.setTitleColor(.white, for: UIControl.State.normal)
        
        office.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        office.titleLabel?.textColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        
        home.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        home.titleLabel?.textColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
    }
     func layoutSubviews() {
      
            searcButton.roundCorners(corners: [.topRight, .bottomRight], radius: 6.0)
       
//        searchText.roundCorners(corners: [.topLeft, .bottomLeft], radius: 6.0)
//        searchText.backgroundColor = .clear
        searchText.layer.borderColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        searchText.layer.borderWidth = 0.5
        searchText.layer.cornerRadius = 6
        searchText.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
extension AddNewViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedWhenInUse else {
            return
        }
        
        locationManager.startUpdatingLocation()
        
        gmsMapView.isMyLocationEnabled = true
        gmsMapView.settings.myLocationButton = true
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.first else {
            return
        }
        
        
        
        gmsMapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        
        locationManager.stopUpdatingLocation()
        
    }
    
}
