//
//  SearchAddressViewController.swift
//  Mirrors
//
//  Created by apple on 24/07/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import GoogleMaps
import GooglePlaces

class SearchAddressViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,GMSMapViewDelegate,UITextFieldDelegate {

    var g_lat: String!
    var g_long: String!
    var g_address: String!
    @IBOutlet weak var tableView1: UITableView!
    @IBOutlet weak var contiu2: UIButton!
    @IBOutlet weak var searchText: UITextField!
    @IBOutlet weak var viewMember: UIButton!
    var mainText = [String]()
    var placeIDArray = [String]()
    var resultsArray = [String]()
    var primaryAddressArray = [String]()
    var searchResults = [String]()
    var searhPlacesName = [String]()
    let googleAPIKey = "AIzaSyA8TbZVvaxhQZ73AwqGROcCvkmdptS6PI4"
//    var delegate: LocateOnTheMap?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView1.separatorStyle = .none
        
        searchText.delegate = self
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainText.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView1.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SearchResultTableViewCell
        
        cell.title?.text = mainText[indexPath.row]
        cell.detail?.text = resultsArray[indexPath.row]
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let correctedAddress = self.resultsArray[indexPath.row].addingPercentEncoding(withAllowedCharacters: .symbols) else {
            print("Error. cannot cast name into String")
            return
        }
            
        let urlString =  "https://maps.googleapis.com/maps/api/geocode/json?address=\(correctedAddress)&sensor=false&key=\(self.googleAPIKey)"
        
        let url = URL(string: urlString)
        
        AF.request(url!, method: .get, headers: nil)
        .validate()
            .responseJSON { [self] (response) in
                
                
                
                
                
                switch response.result {
                case.success(let value):
                    let json = JSON(value)
//                    print(json)
//                    cityName.text = "super"
                    let lat = json["results"][0]["geometry"]["location"]["lat"].rawString()
                    let lng = json["results"][0]["geometry"]["location"]["lng"].rawString()
                    let formattedAddress = json["results"][0]["formatted_address"].rawString()
                    print(formattedAddress)
                    let city = json["results"][0]["address_components"] as? [Any]
                   
                    
                    
                   
                   
//                    droppedPin2.text = formattedAddress
//                    cityName.text = mainText[indexPath.row]
                    
                   
//                            if city == ["administrative_area_level_1","political"]  {
//
//                                print("city nam iw", json["results"][0]["address_components"]["long_name"])
//
//
//
//                    }
                   
//                    getAddress(pdblLatitude: Double(lat!)!, withLongitude: Double(lng!)!)
                   
                    let state = json["results"][0]["formatted_address"][3]["short_name"].rawString()
                    print("stte nam iw", city)
                    g_lat = lat
                    g_long = lng
                    g_address = formattedAddress
//                    addressLbl.text = formattedAddress
//                    self.droppedPin.text = formattedAddress
                   
                    if g_lat != nil {
                        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
//                        locateWithLong(lon: g_lat, andLatitude: g_long, andAddress: g_address)
                    }else{
                        "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
                    }
//                    mapPlaceH.isHidden = false
                    
                    let naVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddNewViewController") as! AddNewViewController
                    naVC.lat = lat!
                    naVC.long = lng!
                    print(lat)
                    print(lng)
                    naVC.formatAddress = formattedAddress!
                    navigationController?.pushViewController(naVC, animated: true)
                    
                    
                case.failure(let error):
                    print("\(error.localizedDescription)")
                }
            
                
        }
        
    }
   
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        print("rt")
        let searchText  = textField.text! + string

        if searchText.count > 0 {
            print("xx")
            tableView1.isHidden = false

            placeAutocomplete(text_input: searchText)
            self.tableView1.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .automatic)

            
        }
        else{
            searchResults = []
            mainText = []
            tableView1.reloadData()
        }

        return true
    }
    @IBAction func cancel(_ sender: Any) {
       
        searchText.text = ""
        searchText.resignFirstResponder()
        self.tableView1.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .automatic)
        
        
    }
    
    
    func placeAutocomplete(text_input: String) {
        let filter = GMSAutocompleteFilter()
        let placesClient = GMSPlacesClient()
        filter.type = .establishment
        
       
        //geo bounds set for bengaluru region
        let bounds = GMSCoordinateBounds(coordinate: CLLocationCoordinate2D(latitude: 13.001356, longitude: 75.174399), coordinate: CLLocationCoordinate2D(latitude: 13.343668, longitude: 80.272055))
        
        placesClient.autocompleteQuery(text_input, bounds: bounds, filter: nil) { (results, error) -> Void in
            self.placeIDArray.removeAll() //array that stores the place ID
            self.resultsArray.removeAll() // array that stores the results obtained
            self.primaryAddressArray.removeAll() //array storing the primary address of the place.
            self.mainText.removeAll()
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
               
                
                for result in results {
                    print(result)
                    self.primaryAddressArray.append(result.attributedPrimaryText.string)
                    //print("primary text: \(result.attributedPrimaryText.string)")
                    //print("Result \(result.attributedFullText) with placeID \(String(describing: result.placeID!))")
                    self.resultsArray.append(result.attributedSecondaryText?.string ?? "nil")
                    self.mainText.append(result.attributedPrimaryText.string)
                    
                    self.primaryAddressArray.append(result.attributedPrimaryText.string)
                    self.placeIDArray.append(result.placeID)
                }
            }
            self.searchResults = self.resultsArray
            self.searhPlacesName = self.primaryAddressArray
            self.tableView1.reloadData()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
