//
//  Address1ViewController.swift
//  Mirrors
//
//  Created by apple on 24/07/21.
//

import UIKit

class Address1ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var change: UIButton!
    @IBOutlet weak var tableView1: UITableView!
    @IBOutlet weak var viewMember: UIButton!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var continue2: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        continue2.layer.cornerRadius = 10
        change.layer.cornerRadius = 8
        textFieldSearch.layer.cornerRadius = 10
        viewMember.layer.cornerRadius = 10
        addNewAddress.layer.cornerRadius = 8
        addNewAddress.layer.borderWidth = 0.5
        addNewAddress.layer.borderColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        
        tableView1.separatorStyle = .none

        // Do any additional setup after loading the view.
    }
    
    
    @IBOutlet weak var addNewAddress: UIButton!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 134
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView1.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddressTableViewCell
        
        
        cell.view1.layer.cornerRadius = 10
        cell.view1.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.view1.layer.shadowColor = #colorLiteral(red: 0.6078431373, green: 0.6588235294, blue: 0.6901960784, alpha: 1)
        cell.view1.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.view1.layer.shadowRadius = 2.0
        cell.view1.layer.shadowOpacity = 1
        cell.view1.layer.borderWidth = 1
        cell.view1.layer.borderColor = #colorLiteral(red: 0.6078431373, green: 0.6588235294, blue: 0.6901960784, alpha: 0.1041577483)
        cell.view1.layer.masksToBounds = true
        
        cell.layer.masksToBounds = false
        
        
        
        return cell
    }
    

   

    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
