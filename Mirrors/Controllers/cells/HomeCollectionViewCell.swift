//
//  HomeCollectionViewCell.swift
//  Mirrors
//
//  Created by apple on 19/07/21.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image1: UIImageView!
    
    
    @IBOutlet weak var title: UILabel!
}
