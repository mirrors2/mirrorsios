//
//  SalonTimViewController.swift
//  Mirrors
//
//  Created by apple on 21/07/21.
//

import UIKit
import Alamofire
import SVProgressHUD
class SalonTimViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource {
    var services = ["HAIR","FACIAL","EYELASH","THREADING","WAX","NAIL"]
    var DetailServices = ["HAIR TREATMENT","HAIR EXTENSION","HAIR ENHACEMENT","HAIR COLOR","WAX","NAIL"]
    @IBOutlet weak var collectionView1: UICollectionView!
    @IBOutlet weak var textFieldSearch: UITextField!
    
    
    @IBOutlet weak var collectionView6: UICollectionView!
    
    var duartion = [Int]()
    var names  = [String]()
    var ids = [Int]()
    var caterogires = [String]()
    var serviceTag = 2
    @IBOutlet weak var finalAdd: UIButton!
    @IBOutlet weak var hygeinicView: UIButton!
    @IBOutlet weak var tableView1: UITableView!
    var val = [0,0,0,0]
    
    @IBOutlet weak var popView: UIView!
    var addBtn2 = [Bool]()
    var addBtn3 = [Bool]()
    @IBOutlet weak var change: UIButton!
    @IBOutlet weak var collectionView2: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        popView.clipsToBounds = true
        popView.layer.cornerRadius = 10
        popView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        
        
        print("fg",caterogires)
        home4(tag:serviceTag)
        home2()
        hygeinicView.layer.cornerRadius = 6
        textFieldSearch.layer.cornerRadius = 6
        finalAdd.layer.cornerRadius = 6
        change.layer.cornerRadius = 6
        
        
        tableView1.separatorStyle = .none
        tableView1.separatorStyle = .none
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      
            let cell = tableView1.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell11
            cell.minus.tag = indexPath.row
            cell.plus.tag = indexPath.row
            cell.add.tag = indexPath.row
           
            cell.minus.addTarget(nil, action: #selector(minus(sender:)), for: .touchUpInside)
            cell.plus.addTarget(nil, action: #selector(plus(sender:)), for: .touchUpInside)
        cell.add.addTarget(nil, action: #selector(add(sender:)), for: .touchUpInside)
        
        
        
        cell.add.layer.cornerRadius = 6
        cell.layer.masksToBounds = true
        
        cell.plus.clipsToBounds = true
            let path = UIBezierPath(roundedRect: cell.plus.bounds,
                                    byRoundingCorners: [.topRight, .bottomRight],
                                    cornerRadii: CGSize(width: 6, height: 6))

            let maskLayer = CAShapeLayer()

            maskLayer.path = path.cgPath
        cell.plus.layer.mask = maskLayer
        
        
        
        cell.minus.clipsToBounds = true
            let path2 = UIBezierPath(roundedRect: cell.minus.bounds,
                                    byRoundingCorners: [.topLeft, .bottomLeft],
                                    cornerRadii: CGSize(width: 6, height: 6))

            let maskLayer2 = CAShapeLayer()

            maskLayer2.path = path2.cgPath
        cell.minus.layer.mask = maskLayer2
        
        
        if addBtn3[indexPath.row] == true {
            cell.value.isHidden = false
            cell.plus.isHidden = false
            cell.minus.isHidden = false
            cell.add.isHidden = true
        }else {
            cell.value.isHidden = true
            cell.plus.isHidden = true
            cell.minus.isHidden = true
            cell.add.isHidden = false
        }
            
         
//        cell.id.text = "\(ids[indexPath.row])"
//        cell.duration.text = "\(duartion[indexPath.row])"
        cell.title.text = names[indexPath.row]
            
            return cell
        }
    @objc func minus(sender: UIButton) {
        let indexPath = IndexPath(item: sender.tag, section: 0)
        if val[indexPath.row] != 0 {
        val[indexPath.row] -= 1
            print(indexPath.item)
        }
        else {
            addBtn3[indexPath.row] = false
           
        }
    self.tableView1.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
    }
    @objc func plus(sender: UIButton) {
      
    let indexPath = IndexPath(item: sender.tag, section: 0)
//        let indexpath = tableView1.indexPath(for: cell)
        val[indexPath.row] += 1
        print(indexPath.item)
       
        self.tableView1.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        
    }
    @objc func add(sender: UIButton) {

        let indexPath = IndexPath(item: sender.tag, section: 0)
        print(indexPath.row)
        if addBtn3[indexPath.row] == false {
            addBtn3[indexPath.row] = true
            print(addBtn3[indexPath.row])
        }
        else {
            print("adhi avvadhama")
        }
                self.tableView1.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 153
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionView1 {
            return caterogires.count
        }
        if collectionView == collectionView2 {
            return DetailServices.count
        }
        if collectionView == collectionView6 {
            return 4
        }
        return 10
    }
    
    
    
   var selectIndex = 0
    var selectIndex2 = 0
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionView2 {
            
            let cell  = collectionView2.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell22
            cell.title.text = DetailServices[indexPath.row]
          
            cell.contentView.layer.cornerRadius = 4
            cell.title.textColor   = .white
            cell.contentView.backgroundColor = .black
            cell.title.backgroundColor  = .black
            
            
            if selectIndex2 == indexPath.row {
                cell.contentView.backgroundColor = .white
                cell.title.backgroundColor  = .white
                cell.title.textColor   = .black
            }
            return cell
        }
        
        if collectionView == collectionView6 {
            
            let cell  = collectionView6.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell55
            
            cell.contentView.layer.cornerRadius = 10
            cell.contentView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.contentView.layer.shadowColor = #colorLiteral(red: 0.6078431373, green: 0.6588235294, blue: 0.6901960784, alpha: 1)
            cell.contentView.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.contentView.layer.shadowRadius = 2.0
            cell.contentView.layer.shadowOpacity = 1
            cell.contentView.layer.borderWidth = 1
            cell.contentView.layer.borderColor = #colorLiteral(red: 0.6078431373, green: 0.6588235294, blue: 0.6901960784, alpha: 0.1041577483)
            cell.contentView.layer.masksToBounds = true
            
            cell.layer.masksToBounds = false
            
            return cell
            
        }
        let cell  = collectionView1.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell11
       
        cell.contentView.backgroundColor = .white
        cell.title.backgroundColor  = .white
        cell.title.textColor   = .black
        cell.title.text = caterogires[indexPath.row]
        
        if #available(iOS 11.0, *) {
            cell.contentView.clipsToBounds = true
            cell.contentView.layer.cornerRadius = 10
            cell.contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            }
        if selectIndex == indexPath.row {
            cell.contentView.backgroundColor = .black
            cell.title.backgroundColor  = .black
            cell.title.textColor   = .white
        }else {
            cell.contentView.backgroundColor = .white
            cell.title.backgroundColor  = .white
            cell.title.textColor   = .black
        }
        
       
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        
        if collectionView == collectionView2 {
            selectIndex2 = indexPath.row
            collectionView2.reloadData()
            
        }
        else if collectionView == collectionView1 {
            selectIndex = indexPath.row

          
          
            print(indexPath.row)
            
        }
        
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
   
    func home2() {
        
         caterogires = [String]()
       
//          lookUpIds = [Int]()
//          lookUpNames = [String]()
//        businesstypes = [String]()
                        SVProgressHUD.setDefaultStyle(.custom)
                        SVProgressHUD.setDefaultMaskType(.custom)
                        SVProgressHUD.setForegroundColor(UIColor.green)           //Ring Color
                        SVProgressHUD.setBackgroundColor(UIColor.clear)
                         SVProgressHUD.show()
       

        
     
var dgdfgkh = "https://api.vineel.tech/user/getcategories.php"
        
 
        AF.request(dgdfgkh, method: .get, parameters:nil ,encoding: JSONEncoding.default, headers: nil).responseJSON { [self]
            response in
          
            switch response.result {
            case .success(let value):
                do {
                  
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    var datas = [[String:Any]]()
                  
                    if let JSON = value as? [String: Any] {
                        print(JSON)
                        if let services = JSON["categories"] as? [[String:Any]] {
                           
                           
                            for i in services  {
                                if  let name = i["categoryname"] as? String {
                                    print(name)
                                    caterogires.append(name)
                                }
                             
//                                if let id = i["id"] as? String {
//
//                                    ids.append(Int(id)!)
//                            }
                            }
                            collectionView1.reloadData()
 
                    }
                        
 
                    }
                    else
                    {
                        print("something went wrong")
                    }
                    }
                    
                    
                }
                break
            case .failure(let error):
                   
                print("failed response ",error)
//                print("sendLatLong Err", error.localizedDescription)
                                SVProgressHUD.showError(withStatus: "Try Again!")
            }
        }
        
    }
    
    func home4(tag:Int) {
         duartion = [Int]()
         names  = [String]()
         ids = [Int]()
        addBtn3 = [Bool]()
        addBtn2 = [Bool]()
//          lookUpIds = [Int]()
//          lookUpNames = [String]()
//        businesstypes = [String]()
                        SVProgressHUD.setDefaultStyle(.custom)
                        SVProgressHUD.setDefaultMaskType(.custom)
                        SVProgressHUD.setForegroundColor(UIColor.green)           //Ring Color
                        SVProgressHUD.setBackgroundColor(UIColor.clear)
                         SVProgressHUD.show()
       
      
     
let dgdfgkh = "https://api.vineel.tech/user/getservices.php?category_id=\(tag)&location_id=4"
        
 
        AF.request(dgdfgkh, method: .post, parameters:nil ,encoding: JSONEncoding.default, headers: nil).responseJSON { [self]
            response in
          
            switch response.result {
            case .success(let value):
                do {
                  
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    var datas = [[String:Any]]()
                  
                    if let JSON = value as? [String: Any] {
                     
                        if let services = JSON["services"] as? [[String:Any]] {
                           
                            
                            for i in services  {
                                addBtn2.append(false)
                                addBtn3.append(false)
                                if  let name = i["name"] as? String {
                                    print(name)
                                names.append(name)
                                }
                                if let duration = i["duration"] as? Int {
                                duartion.append(duration)
                            }
                                if let id = i["id"] as? Int {
                                    print(id)
                                   ids.append(id)
                            }
                            }
                            tableView1.reloadData()
                            collectionView1.reloadData()
                    }
                    else
                    {
                        print("something went wrong")
                    }
                    }
                    
                    
                }
                }
                break
            case .failure(let error):
                   
                print("failed response ",error)
//                print("sendLatLong Err", error.localizedDescription)
                                SVProgressHUD.showError(withStatus: "Try Again!")
            }
        }
        
    }
    
    
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if collectionView == collectionView2 {
//            let label = UILabel(frame: CGRect.zero)
//            label.text = DetailServices[indexPath.item]
//            label.textAlignment = .center
//            label.sizeToFit()
//            return CGSize(width: label.frame.width+20, height: 40)
//        }
//
//        else {
//        let label = UILabel(frame: CGRect.zero)
//        label.text = services[indexPath.item]
//        label.textAlignment = .center
//        label.sizeToFit()
//        return CGSize(width: label.frame.width+20, height: 25)
//        }
//    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
