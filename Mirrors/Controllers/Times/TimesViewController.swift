//
//  TimesViewController.swift
//  Mirrors
//
//  Created by apple on 26/07/21.
//

import UIKit

class TimesViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var contiu2: UIButton!
    
    @IBOutlet weak var viewMember: UIButton!
    
    @IBOutlet weak var collectionView2: UICollectionView!
    @IBOutlet weak var collectionView1: UICollectionView!
    
    var fullDate = [String]()
    var today = ""
    
    var times = ["10:00 Am","10:30 AM","11:00 AM","11:30 AM" ,"12:00 PM" ,"12:30 PM" ,"1:00 PM","1:30 PM","2:00 PM","2:30 PM","3:00 PM","3:30 PM","4:00 PM","4:30 PM","5:00 PM","6:30 PM","7:00 PM","7:30 PM","8:00 PM","8:30 PM","9:00 PM","9:30 PM"]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionView2 {
            return times.count
        }
        return days.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionView2 {
            let cell = collectionView2.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TC2CollectionViewCell
            cell.time.text = times[indexPath.row]
            if selectIndexPath2 == indexPath {
                print("yes")
               
                cell.view1.layer.cornerRadius = 6
                cell.view1.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner ,.layerMaxXMaxYCorner , .layerMaxXMinYCorner]
                
                cell.view1.backgroundColor = #colorLiteral(red: 1, green: 0.6804133058, blue: 0.6915783286, alpha: 1)
                cell.time.textColor = .white
            }
            else
            {
            
            cell.view1.layer.borderColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
            cell.time.textColor = .black
            cell.view1.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.view1.layer.borderWidth = 0.5
            cell.view1.layer.cornerRadius = 6
            cell.view1.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner ,.layerMaxXMaxYCorner , .layerMaxXMinYCorner]
            }
            return cell
        }
        let cell = collectionView1.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TCollectionViewCell
        cell.day.text = days[indexPath.row]
        cell.date.text = dates[indexPath.row]
        if selectIndexPath == indexPath {
            cell.view1.backgroundColor = #colorLiteral(red: 1, green: 0.6804133058, blue: 0.6915783286, alpha: 1)
            cell.view1.layer.cornerRadius = 6
            cell.day.textColor = .white
            cell.date.textColor = .white
            cell.view1.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner ,.layerMaxXMaxYCorner , .layerMaxXMinYCorner]
        }
        
        else {
            cell.view1.layer.borderColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
            cell.view1.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.day.textColor = #colorLiteral(red: 0.8406969905, green: 0.8407167792, blue: 0.8407061696, alpha: 1)
            cell.date.textColor = .black
            cell.view1.layer.borderWidth = 0.5
            cell.view1.layer.cornerRadius = 6
            cell.view1.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner ,.layerMaxXMaxYCorner , .layerMaxXMinYCorner]
            
        }
        
        
       
        
        
        return cell
    }
    var selectIndexPath:IndexPath?
    var selectIndexPath2:IndexPath?
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionView2 {
          
        selectIndexPath2 = indexPath
           
             collectionView2.reloadData()
        }
        else if collectionView == collectionView1 {
            selectIndexPath = indexPath
//            today = fullDate[indexPath.row]
            appointTimes(selectDate: fullDate[indexPath.row])
            collectionView1.reloadData()
            
        }
            
    }
    
      var dateFormatter: DateFormatter = {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    @IBOutlet weak var change: UIButton!
    
    @IBOutlet weak var home: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let formatter3 = DateFormatter()
        formatter3.dateFormat = "dd-MM-yyyy"

        let startDate = formatter3.string(from: Date())

        appointTimes(selectDate: startDate)
        
        
        let today = Date()
        let nextFiveDays = Calendar.current.date(byAdding: .day, value: +31, to: today)!
        let nextFiveDays2 = Calendar.current.date(byAdding: .day, value: -1, to: today)!

        let myRange = datesRange(from: nextFiveDays2, to: nextFiveDays)
        print(myRange)
        // Do any additional setup after loading the view.
    }
    
    func appointTimes(selectDate:String) {
    var array: [String] = []

    let formatter = DateFormatter()
    formatter.dateFormat = "dd-MM-yyyy hh:mm a"

    let formatter3 = DateFormatter()
    formatter3.dateFormat = "dd-MM-yyyy"

    
    let formatter2 = DateFormatter()
    formatter2.dateFormat = "hh:mm a"
    
    let formatter6 = DateFormatter()
    formatter6.dateFormat = "hh:"
    
    let formatter7 = DateFormatter()
    formatter7.dateFormat = "a"
    
    let formatter8 = DateFormatter()
    formatter8.dateFormat = "mm"
    
     
    let endDate = formatter3.string(from: Date())
    
    
    let timw4 = formatter6.string(from: Date())
    let timw5 = formatter7.string(from: Date())
    
    let time6 = formatter8.string(from: Date())
//        let Curdate = dt.DateToString(Formatter: "dd-MM-yyyy")
//                let Nxtdate = dt.DateToString(Formatter: "dd-MM-yyyy")
//
        print(selectDate)
        print(endDate)
    if selectDate == endDate {
    if time6 < "30" {
    let startDate1 = "\(selectDate) \(timw4)00 \(timw5)"
            let endDate1 = "\(selectDate) 09:30 PM"
    
    let date1 = formatter.date(from: startDate1)
    let date2 = formatter.date(from: endDate1)

    var i = 1
    while true {
        let date = date1?.addingTimeInterval(TimeInterval(i*30*60))
        let string = formatter2.string(from: date!)
        
        if date! >= date2! {
            break;
        }

        i += 1
        array.append(string)
    }
    print(array)
    
    times = array
        print(times)
    collectionView2.reloadData()
    }
    else {
        let startDate1 = "\(selectDate) \(timw4)30 \(timw5)"
        let endDate1 = "\(selectDate) 09:30 PM"
        
        let date1 = formatter.date(from: startDate1)
        let date2 = formatter.date(from: endDate1)

        var i = 1
        while true {
            let date = date1?.addingTimeInterval(TimeInterval(i*30*60))
            let string = formatter2.string(from: date!)
           
            
            if date! >= date2! {
                break;
            }

            i += 1
            array.append(string)
        }
        print(array)
        
        times = array
        collectionView2.reloadData()
        }
    }
    else {
        
        
        let startDate1 = "\(selectDate) 10:00 AM"
                let endDate1 = "\(selectDate) 09:30 PM"
        print(startDate1)
        print(endDate1)
        print(timw5)
            print(time6)
        let date1 = formatter.date(from: startDate1)
        let date2 = formatter.date(from: endDate1)

        var i = 1
        while true {
            let date = date1?.addingTimeInterval(TimeInterval(i*30*60))
            let string = formatter2.string(from: date!)
            
            if date! >= date2! {
                break;
            }

            i += 1
            array.append(string)
        }
        print(array)
        
        times = array
        collectionView2.reloadData()
    }
   }
  
     var allDays = [Int]()
      var weeks = 0
      var totalDaysInMonth = 0
     var items = [[Date]]()
    
    
      var days = [String]()
        var dates = [String]()
      func datesRange(from: Date, to: Date) -> [Date] {
          // in case of the "from" date is more than "to" date,
          // it should returns an empty array:
          if from > to { return [Date]() }

          var tempDate = from
          var array = [tempDate]

          while tempDate < to {
              tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
              array.append(tempDate)
              
          
              let dateFormatter = DateFormatter()
              dateFormatter.dateFormat = "EEEE"
            
            let dateFormatter3 = DateFormatter()
            dateFormatter3.dateFormat = "dd-MM-yyyy"
            
            let date5 = dateFormatter3.string(from: tempDate)
            fullDate.append(date5)
            
              let day1 = dateFormatter.string(from: tempDate)
              days.append(day1)
              
              
              let dateFormatter2 = DateFormatter()
              dateFormatter2.dateFormat = "dd"
              let date = dateFormatter2.string(from: tempDate)
              dates.append(date)
              print("date is",date)
              
              print(day1)
              
          }
        print(fullDate)
        collectionView1.reloadData()
          return array
      }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
