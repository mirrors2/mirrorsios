//
//  AccountsViewController.swift
//  Mirrors
//
//  Created by apple on 28/07/21.
//

import UIKit

class AccountsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView1: UITableView!
    
    var mainText = ["My Dashboard","My Bookings","My Retail Orders","About us","Refer and Earn","Rate to Mirrors","Contact us","Share app with others"]
    
    @IBOutlet weak var tablevewiH: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView1.separatorStyle = .none
        tableView1.reloadData()
        tableView1.layoutIfNeeded()
        tablevewiH.constant = tableView1.contentSize.height
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainText.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2 {
            return 60
        }
        
        return 42
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView1.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ATableViewCell
        
        cell.title?.text = mainText[indexPath.row]
       
        if indexPath.row == 2 {
            cell.label1.isHidden = false
        }
        else {
            cell.label1.isHidden = true
        }
        cell.selectionStyle = .none
        
        return cell
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
