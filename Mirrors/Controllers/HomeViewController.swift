//
//  HomeViewController.swift
//  Mirrors
//
//  Created by apple on 16/07/21.
//

import UIKit
import SVProgressHUD
import Alamofire

class HomeViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView5: UICollectionView!
    @IBOutlet weak var collectiionView4: UICollectionView!
    @IBOutlet weak var collectionView3: UICollectionView!
    @IBOutlet weak var collectionView2: UICollectionView!
    
    @IBOutlet weak var chatWithus: UIButton!
    
    @IBOutlet weak var service: UIButton!
    
    @IBOutlet weak var retail: UIButton!
    
    @IBOutlet weak var viewMember: UIButton!
    
    
     override func viewDidLoad() {
         super.viewDidLoad()
        chatWithus.layer.cornerRadius = 10
        service.layer.cornerRadius = 10
        retail.layer.cornerRadius = 8
        viewMember.layer.cornerRadius = 8
        home2()
        home3()
      
        setTimer()
         self.pageControl.numberOfPages = 8
 //        self.view.addSubview(collectionView)
 //        self.collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
 //        self.collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
 //        self.collectionView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
 //        self.collectionView.heightAnchor.constraint(equalToConstant: cellWidth).isActive = true
 //
         // Do any additional setup after loading the view.
     }
    //    lazy var collectionView:UICollectionView = {
//       let layOut = PagingCollectionViewLayout()
//        layOut.sectionInset = .init(top: 0, left: spacing, bottom: 0, right: spacing)
//
//        layOut.minimumLineSpacing = cellSpacing
//        layOut.itemSize = .init(width: cellWidth, height: cellWidth)
//        layOut.scrollDirection = .horizontal
//
//        let collctionView = UICollectionView(frame: .zero, collectionViewLayout: layOut)
//        collctionView.translatesAutoresizingMaskIntoConstraints = false
//        collctionView.showsHorizontalScrollIndicator = false
//        collectionView1.decelerationRate = .fast
//        collctionView.dataSource = self
//        collctionView.backgroundColor = .white
//        collctionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
//        return collctionView
//    }()

    @IBOutlet weak var collectionView1: UICollectionView!
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionView2 {
            return names.count
        }
        if collectionView == collectionView5 {
            return 8
        }
        if collectionView == collectiionView4 {
            return 9
        }
        if collectionView == collectionView3 {
            return 5
        }
       return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionView2 {
            let cell = collectionView2.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeCollectionViewCell
            
            let naVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SalonTimViewController") as! SalonTimViewController
            naVC.serviceTag = ids[indexPath.row]
            
            self.navigationController?.pushViewController(naVC, animated: true)
            print(ids[indexPath.row])
        }
        if collectionView == collectionView1 {
//            let cell = collectionView2.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeCollectionViewCell
            
            let naVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SalonTimViewController") as! SalonTimViewController
             names = naVC.caterogires
            
//            naVC.serviceTag = cell.tag
            self.navigationController?.pushViewController(naVC, animated: true)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionView2 {
            let cell = collectionView2.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeCollectionViewCell
            
            cell.image1.layer.cornerRadius = cell.image1.frame.width/2
            cell.title.text = names[indexPath.row]
             cell.tag =  ids[indexPath.row]
            print(cell.tag)
           
            return cell
        }
        
        
        
        if collectionView == collectionView5 {
            let cell = collectionView5.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell5
            if indexPath.row == 2 {
            cell.image1.backgroundColor = UIColor.red
            }
            return cell
        }
        if collectionView == collectiionView4 {
            let cell = collectiionView4.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell4
            
           
           
            return cell
        }
        
        
        if collectionView == collectionView3 {
            let cell = collectionView3.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell3
        
            return cell
        }
        
        
        let cell = collectionView1.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell1
     
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionView2 {
            return CGSize(width: 57, height: 77)
        }
        if collectionView == collectiionView4 {
            return CGSize(width: 113, height: 59)
        }
        if collectionView == collectionView3 {
            return CGSize(width: 135, height: 81)
        }
        if collectionView == collectionView5 {
            let frameSize = collectionView.frame.size
            return CGSize(width: frameSize.width, height: frameSize.height)
        }
        return CGSize(width: 300, height: 335)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: self.collectionView5.contentOffset, size: self.collectionView5.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let visibleIndexPath = self.collectionView5.indexPathForItem(at: visiblePoint) {
            self.pageControl.currentPage = visibleIndexPath.row
        }
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {

        let pageWidth = Float(UIScreen.main.bounds.width + 0)
        let targetXContentOffset = Float(targetContentOffset.pointee.x)
        let contentWidth = Float(collectionView5!.contentSize.width  )
        var newPage = Float(self.pageControl.currentPage)

        if velocity.x == 0 {
            newPage = floor( (targetXContentOffset - Float(pageWidth) / 2) / Float(pageWidth)) + 1.0
        } else {
            newPage = Float(velocity.x > 0 ? self.pageControl.currentPage + 1 : self.pageControl.currentPage - 1)
            if newPage < 0 {
                newPage = 0
            }
            if (newPage > contentWidth / pageWidth) {
                newPage = ceil(contentWidth / pageWidth) - 1.0
            }
        }

        self.pageControl.currentPage = Int(newPage)
        let point = CGPoint (x: CGFloat(newPage * pageWidth), y: targetContentOffset.pointee.y)
        targetContentOffset.pointee = point
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectiionView4 {
            return 10
        }
        if collectionView == collectionView3 {
            return 10
        }
           return 0
       }

      

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

           return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
       }
   
    // CollectionViewFlowLayoutDelegate

    
    var names = [String]()
    var ids = [Int]()
   
    func home2() {
        
         names = [String]()
         ids = [Int]()
//          lookUpIds = [Int]()
//          lookUpNames = [String]()
//        businesstypes = [String]()
                        SVProgressHUD.setDefaultStyle(.custom)
                        SVProgressHUD.setDefaultMaskType(.custom)
                        SVProgressHUD.setForegroundColor(UIColor.green)           //Ring Color
                        SVProgressHUD.setBackgroundColor(UIColor.clear)
                         SVProgressHUD.show()
       

        
     
var dgdfgkh = "https://api.vineel.tech/user/getcategories.php"
        
 
        AF.request(dgdfgkh, method: .get, parameters:nil ,encoding: JSONEncoding.default, headers: nil).responseJSON { [self]
            response in
          
            switch response.result {
            case .success(let value):
                do {
                  
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    var datas = [[String:Any]]()
                  
                    if let JSON = value as? [String: Any] {
                        print(JSON)
                        if let services = JSON["categories"] as? [[String:Any]] {
                           
                           
                            for i in services  {
                                if  let name = i["categoryname"] as? String {
                                    print(name)
                                names.append(name)
                                }
                             
                                if let id = i["id"] as? String {
                                   
                                    ids.append(Int(id)!)
                            }
                            }
                            collectionView2.reloadData()
 
                    }
                        
 
                    }
                    else
                    {
                        print("something went wrong")
                    }
                    }
                    
                    
                }
                break
            case .failure(let error):
                   
                print("failed response ",error)
//                print("sendLatLong Err", error.localizedDescription)
                                SVProgressHUD.showError(withStatus: "Try Again!")
            }
        }
        
    }
    func home3() {
//          lookUpIds = [Int]()
//          lookUpNames = [String]()
//        businesstypes = [String]()
                        SVProgressHUD.setDefaultStyle(.custom)
                        SVProgressHUD.setDefaultMaskType(.custom)
                        SVProgressHUD.setForegroundColor(UIColor.green)           //Ring Color
                        SVProgressHUD.setBackgroundColor(UIColor.clear)
                         SVProgressHUD.show()
       

        
     
var dgdfgkh = "https://api.vineel.tech/user/getlocation.php"
        
 
        AF.request(dgdfgkh, method: .get, parameters:nil ,encoding: JSONEncoding.default, headers: nil).responseJSON { [self]
            response in
          
            switch response.result {
            case .success(let value):
                do {
                  
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    var datas = [[String:Any]]()
                  
                    if let JSON = value as? [String: Any] {
                        print(JSON)
                        
 
                    }
                    else
                    {
                        print("something went wrong")
                    }
                    }
                    
                    
                }
                break
            case .failure(let error):
                   
                print("failed response ",error)
//                print("sendLatLong Err", error.localizedDescription)
                                SVProgressHUD.showError(withStatus: "Try Again!")
            }
        }
        
    }
  
    
    func setTimer() {
         let _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
    }
    var x = 1
    @objc func autoScroll() {
        if self.x < 10 {
          let indexPath = IndexPath(item: x, section: 0)
          self.collectionView1.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
          self.x = self.x + 1
        }else{
          self.x = 0
          self.collectionView1.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//class PagingCollectionViewLayout: UICollectionViewFlowLayout {
//
//    var theVelocityThresholdPerPage: CGFloat = 2
//    var numOfItemsPerPage: CGFloat = 1
//
//    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
//        guard let collectionView = collectionView else { return proposedContentOffset }
//
//        let pageLength: CGFloat
//        let approxPage: CGFloat
//        let currentPage: CGFloat
//        let speed: CGFloat
//
//        if scrollDirection == .horizontal {
//            pageLength = (self.itemSize.width + self.minimumLineSpacing) * numOfItemsPerPage
//            approxPage = collectionView.contentOffset.x / pageLength
//            speed = velocity.x
//        } else {
//            pageLength = (self.itemSize.height + self.minimumLineSpacing) * numOfItemsPerPage
//            approxPage = collectionView.contentOffset.y / pageLength
//            speed = velocity.y
//        }
//
//        if speed < 0 {
//            currentPage = ceil(approxPage)
//        } else if speed > 0 {
//            currentPage = floor(approxPage)
//        } else {
//            currentPage = round(approxPage)
//        }
//
//        guard speed != 0 else {
//            if scrollDirection == .horizontal {
//                return CGPoint(x: currentPage * pageLength, y: 0)
//            } else {
//                return CGPoint(x: 0, y: currentPage * pageLength)
//            }
//        }
//
//        var nextPage: CGFloat = currentPage + (speed > 0 ? 1 : -1)
//
//        let increment = speed / theVelocityThresholdPerPage
//        nextPage += (speed < 0) ? ceil(increment) : floor(increment)
//
//        if scrollDirection == .horizontal {
//            return CGPoint(x: nextPage * pageLength, y: 0)
//        } else {
//            return CGPoint(x: 0, y: nextPage * pageLength)
//        }
//    }
//}
