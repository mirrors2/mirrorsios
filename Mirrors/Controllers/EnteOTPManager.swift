//
//  EnteOTPManager.swift
//  Stylsa Partner
//
//  Created by apple on 16/06/21.
//

import Foundation
import UIKit


class OTPtextfieldManager: UITextField {
    var didEnterLastDigit : ((String) -> Void)?
    
    private var isConfigured = false
    private var digitLabel = [UILabel]()
    private var digiViews = [UIView]()
    
    private lazy var tapGestureRecogniser:UITapGestureRecognizer = {
        
        let recognizer = UITapGestureRecognizer()
        
        recognizer.addTarget(self, action: #selector(self.becomeFirstResponder))
        return recognizer
    }()
    
    
    
    func configure(with slotCount: Int = 5 ) {
        guard isConfigured == false else {return}
        isConfigured.toggle()
        configureTexgField()
        let labelstackView = createLabelStackView(with: slotCount)
        addSubview(labelstackView)
        
        addGestureRecognizer(tapGestureRecogniser)
        NSLayoutConstraint.activate([
            labelstackView.topAnchor.constraint(equalTo: topAnchor),
            labelstackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            labelstackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            labelstackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
    }
    
    
    private func configureTexgField(){
        tintColor = .clear
        keyboardType = .numberPad
        if #available(iOS 12.0, *) {
            textContentType = .oneTimeCode
        } else {
            // Fallback on earlier versions
        }
        
        addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        delegate = self
        }
    
    private func createLabelStackView (with count:Int) -> UIStackView {
        
        let stackView = UIStackView()
        stackView.frame = CGRect(x: 0, y: 61, width: self.frame.width-20, height: 1)
        stackView.translatesAutoresizingMaskIntoConstraints = true
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 8
        
       
         
        for _ in 1...count {
            
            
            let uiview = UIView()
            uiview.frame = CGRect(x: 0, y: 0, width: 40, height: 50)
            uiview.translatesAutoresizingMaskIntoConstraints = true
            uiview.backgroundColor = .gray
          
           
            
            
            let label = UILabel()
            label.frame = CGRect(x: uiview.frame.width/2.5, y: -50, width: 40, height: 50)
            label.translatesAutoresizingMaskIntoConstraints = true
            label.textAlignment = .center
            label.font = .systemFont(ofSize: 40)
            label.backgroundColor = .clear
            label.layer.masksToBounds = true
            label.isUserInteractionEnabled = true
            
           
           
            
           uiview.addSubview(label)
            
            stackView.addArrangedSubview(uiview)
            digiViews.append(uiview)
            digitLabel.append(label)
        }
        
        
        
        
        return stackView
    }
    
    @objc func textDidChange (){
        guard let text = self.text , text.count <= digitLabel.count else {return}

        for i in 0..<digitLabel.count {
            let currentLabel = digitLabel[i]
            
            if i < text.count {
                let index = text.index(text.startIndex, offsetBy: i)
                currentLabel.text = String(text[index])
                currentLabel.textColor = .black

            }else {
//                currentLabel.textColor = .clear
                currentLabel.text = "-"
                currentLabel.textColor = .clear
            }

        }
        for i in 0..<digiViews.count {
            let currentLabel = digiViews[i]
            
            if i < text.count {
                let index = text.index(text.startIndex, offsetBy: i)
               
                currentLabel.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.5529411765, blue: 0.5803921569, alpha: 1)

            }else {
//                currentLabel.textColor = .clear
                currentLabel.backgroundColor = .gray
            }

        }

        if text.count == digitLabel.count {
            didEnterLastDigit?(text)
        }
    }
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}



extension OTPtextfieldManager:UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        
        guard let characterCount = textField.text?.count else {return false}
   print("print count",characterCount)
        return characterCount < 5 || string == ""
    }
    
}
