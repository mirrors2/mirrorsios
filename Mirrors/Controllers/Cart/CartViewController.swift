//
//  CartViewController.swift
//  Mirrors
//
//  Created by apple on 23/07/21.
//

import UIKit

class CartViewController: UIViewController,UITableViewDelegate,UITableViewDataSource  {
    @IBOutlet weak var hygeinicView: UIButton!
    @IBOutlet weak var change: UIButton!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var view1: UIView!
    
    @IBOutlet weak var contiu2: UIButton!
    
    @IBOutlet weak var imageView1: UIImageView!
    
    @IBOutlet weak var savingView: UIButton!
    @IBOutlet weak var viewMember: UIButton!
    
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var tableview1: UITableView!
    
    @IBOutlet weak var tableView4: UITableView!
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var popView: UIView!
    
    
    var loactionNames = ["Home Service",
                         "Al Muraqqabat",
                        "IBN Batuta",
                         "Bur-Dubai",
                         "JLT",
                         "Sharjha"
    ]
    var val = [0,0]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview1.separatorStyle = .none
        tableView4.separatorStyle = .none
         contiu2.layer.cornerRadius = 6
        viewMember.layer.cornerRadius = 6
        hygeinicView.layer.cornerRadius = 6
        savingView.layer.cornerRadius = 10
        viewImage.layer.cornerRadius = 10
        
        contiu2.layer.cornerRadius = 10
        textFieldSearch.layer.cornerRadius = 6
        change.layer.cornerRadius = 6
        view1.layer.borderWidth = 0.5
        view1.layer.borderColor = #colorLiteral(red: 0.8039215686, green: 0.8039215686, blue: 0.8039215686, alpha: 1)
        
        view1.layer.cornerRadius = 6
        
        popView.isHidden = true
        if #available(iOS 11.0, *) {
                self.popView.clipsToBounds = true
            popView.layer.cornerRadius = 25
            popView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            
            
         
          
            
            }
        
        else {
                // Fallback on earlier versions
            }
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        popView.translatesAutoresizingMaskIntoConstraints = true

        popView.center.y += popView.bounds.height
    }
    
    
    @IBOutlet weak var popviewHeight: NSLayoutConstraint!
    @IBAction func changeLocation(_ sender: Any) {
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.alpha = 1
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        bgView.addSubview(blurEffectView)
        
        popView.translatesAutoresizingMaskIntoConstraints = false
        popviewHeight.constant = UIScreen.main.bounds .height - 150
      
        
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseIn],
                       animations: { [self] in
                        popView.center.y -= popView.bounds.height
                        popView.layoutIfNeeded()
        }, completion: nil)
        popView.isHidden = false
      
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableView4 {
            return loactionNames.count
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableView4 {
            let cell = tableView4.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChangeLocTableViewCell
            cell.title.text = loactionNames[indexPath.row]
            return cell
        }
      
            let cell = tableview1.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CartTableViewCell
            cell.minus.tag = indexPath.row
            cell.plus.tag = indexPath.row
           
           
            cell.minus.addTarget(nil, action: #selector(minus(sender:)), for: .touchUpInside)
            cell.plus.addTarget(nil, action: #selector(plus(sender:)), for: .touchUpInside)
     
        
        
        
    
        cell.layer.masksToBounds = true
        
        cell.plus.clipsToBounds = true
            let path = UIBezierPath(roundedRect: cell.plus.bounds,
                                    byRoundingCorners: [.topRight, .bottomRight],
                                    cornerRadii: CGSize(width: 6, height: 6))

            let maskLayer = CAShapeLayer()

            maskLayer.path = path.cgPath
        cell.plus.layer.mask = maskLayer
        
        
        
        cell.minus.clipsToBounds = true
            let path2 = UIBezierPath(roundedRect: cell.minus.bounds,
                                    byRoundingCorners: [.topLeft, .bottomLeft],
                                    cornerRadii: CGSize(width: 6, height: 6))

            let maskLayer2 = CAShapeLayer()

            maskLayer2.path = path2.cgPath
        cell.minus.layer.mask = maskLayer2
        
        
      
            
         
//        cell.id.text = "\(ids[indexPath.row])"
//        cell.duration.text = "\(duartion[indexPath.row])"
     
            
            return cell
        }
    
    
    
    @IBAction func cancelPop(_ sender: Any) {
        print("hiden")
         UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseIn],
                        animations: { [self] in
                         popView.center.y += popView.bounds.height
                         popView.layoutIfNeeded()
         }, completion: nil)
         popView.isHidden = false
       
         popView.translatesAutoresizingMaskIntoConstraints = true
         bgView.removeBlr()
        
        
    }
    @objc func minus(sender: UIButton) {
        let indexPath = IndexPath(item: sender.tag, section: 0)
        if val[indexPath.row] != 0 {
        val[indexPath.row] -= 1
            print(indexPath.item)
        }
        else {
            
           
        }
    self.tableview1.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
    }
    @objc func plus(sender: UIButton) {
      
    let indexPath = IndexPath(item: sender.tag, section: 0)
//        let indexpath = tableView1.indexPath(for: cell)
        val[indexPath.row] += 1
        print(indexPath.item)
       
        self.tableview1.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableView4 {
            return 50
        }
        return 115
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIView {
    func removeBlr() {
    for view in self.subviews {
      if let view = view as? UIVisualEffectView {
        view.removeFromSuperview()
      }
    }
    }
}
