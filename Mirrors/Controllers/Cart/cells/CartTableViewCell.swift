//
//  CartTableViewCell.swift
//  Mirrors
//
//  Created by apple on 23/07/21.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    
    @IBOutlet weak var minus: UIButton!
    @IBOutlet weak var plus: UIButton!
    @IBOutlet weak var value: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.layer.borderWidth = 0.4
        self.contentView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.contentView.layer.cornerRadius = 8
        plus.clipsToBounds = true
            let path = UIBezierPath(roundedRect: plus.bounds,
                                    byRoundingCorners: [.topRight, .bottomRight],
                                    cornerRadii: CGSize(width: 6, height: 6))

            let maskLayer = CAShapeLayer()

            maskLayer.path = path.cgPath
        plus.layer.mask = maskLayer
        
        
        
        minus.clipsToBounds = true
            let path2 = UIBezierPath(roundedRect: minus.bounds,
                                    byRoundingCorners: [.topLeft, .bottomLeft],
                                    cornerRadii: CGSize(width: 6, height: 6))

            let maskLayer2 = CAShapeLayer()

            maskLayer2.path = path2.cgPath
      minus.layer.mask = maskLayer2
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
