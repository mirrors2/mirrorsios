//
//  OTPViewController.swift
//  Mirrors
//
//  Created by apple on 16/07/21.
//

import UIKit

class OTPViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var otpText: OTPtextfieldManager!
    
    
    @IBOutlet weak var successView: UIView!
    
    @IBOutlet weak var shodowView: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        shodowView.isHidden = true
        successView.isHidden = true
        successView.layer.cornerRadius = 14
        otpView.layer.cornerRadius = 14
        loginBtn.layer.cornerRadius = 10
        otpText.delegate = self
        otpText.configure()
//        otpText.becomeFirstResponder()
        
        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func loginAct(_ sender: Any) {
        
        
                let naVC = UIStoryboard(name: "TabViews", bundle: nil).instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
        naVC.selectedIndex = 0
                self.navigationController?.pushViewController(naVC, animated: true)
       
        
        
//        otpText.resignFirstResponder()
//        shodowView.isHidden = false
//        shodowView.backgroundColor = .black
//        shodowView.layer.opacity = 0.5
//        successView.isHidden = false
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
